open System
open Microsoft.AspNetCore.Builder
open Microsoft.AspNetCore.Hosting
open Microsoft.Extensions.Hosting
open Microsoft.Extensions.DependencyInjection
open Giraffe
open Microsoft.Extensions.Logging
open Microsoft.AspNetCore.Authentication
open Microsoft.AspNetCore.Authentication.Cookies
open Microsoft.AspNetCore.Http
open FSharp.Control.Tasks.V2.ContextInsensitive
open System.Security.Claims

let mutable users = Map<string, string * string>([])

let errorHandler (ex: Exception) (logger: ILogger) =
    logger.LogError(EventId(), ex, "An unhandled exception has occured while executing the request")

    clearResponse
    >=> setStatusCode 500
    >=> text ex.Message

let tryBindJson<'T> (parsingErrorHandler: string -> HttpHandler) (successHandler: 'T -> HttpHandler) : HttpHandler =
    fun (next: HttpFunc) (ctx: HttpContext) ->
        task {
            try
                let! model = ctx.BindJsonAsync<'T>()
                return! successHandler model next ctx
            with ex ->
                let logger = ctx.GetLogger("tryBindJson")
                logger.LogError(EventId(), ex, "Malformed JSON in request")
                return! parsingErrorHandler "Malformed JSON or missing field in request body" next ctx
        }

[<CLIMutable>]
type LoginUser =
    { username: string
      password: string }
    interface IModelValidation<LoginUser> with
        member this.Validate() =
            let result =
                if this.username = null || this.username.Length < 4 then
                    "username is blank or too short"
                else if this.password = null || this.password.Length < 4 then
                    "password is blank or too short"
                else
                    ""

            match result with
            | "" -> Ok this
            | _ -> Error(RequestErrors.badRequest (text result))


let authScheme =
    CookieAuthenticationDefaults.AuthenticationScheme

let accessDenied : HttpHandler =
    setStatusCode 401
    >=> json """{"message": "Access denied"}"""

let handleBadJsonRequest (error: string) =
    fun (next: HttpFunc) (ctx: HttpContext) -> RequestErrors.BAD_REQUEST error next ctx

let registerUserHandler user =
    fun (next: HttpFunc) (ctx: HttpContext) ->
        task {
            // validate user details
            if (users.ContainsKey user.username) then
                return! RequestErrors.BAD_REQUEST """{"message": "username already exists"}""" next ctx
            else
                users <- users.Add(user.username, (user.password, "salt"))
                return! json """{"message": "Successfully registered the user"}""" next ctx
        }

let loginUserHandler user =
    fun (next: HttpFunc) (ctx: HttpContext) ->
        task {
            let issuer = "http://localhost:5000"

            if (not (users.ContainsKey user.username)) then
                return! RequestErrors.BAD_REQUEST """{"message": "user not found"}""" next ctx
            else
                let claims =
                    [ Claim(ClaimTypes.Name, user.username, ClaimValueTypes.String, issuer)
                      Claim(ClaimTypes.Surname, user.password, ClaimValueTypes.String, issuer) ]

                let identity = ClaimsIdentity(claims, authScheme)
                let user = ClaimsPrincipal(identity)

                do! ctx.SignInAsync(authScheme, user)

                return! json """{"message": "Successfully logged in"}""" next ctx

        }

let mustBeUser = requiresAuthentication accessDenied

let userHandler =
    fun (next: HttpFunc) (ctx: HttpContext) -> text ctx.User.Identity.Name next ctx

let webApp =
    choose [ GET
             >=> choose [ route "/ping" >=> text "pong"
                          route "/" >=> Successful.OK """{"version": 0.1}"""
                          // route "/login" >=> loginHandler
                          route "/user" >=> mustBeUser >=> userHandler ]
             POST
             >=> choose [ route "/login-user"
                          >=> bindJson<LoginUser> loginUserHandler
                          route "/register-user"
                          >=> tryBindJson<LoginUser> handleBadJsonRequest (validateModel registerUserHandler) ] ]

let cookieAuth (options: CookieAuthenticationOptions) =
    do
        options.Cookie.HttpOnly <- true
        options.Cookie.SecurePolicy <- CookieSecurePolicy.SameAsRequest
        options.SlidingExpiration <- true
        options.ExpireTimeSpan <- TimeSpan.FromDays 7.0

let configureServices (services: IServiceCollection) =
    // Add Giraffe dependencies
    services
        .AddGiraffe()
        .AddAuthentication(authScheme)
        .AddCookie(cookieAuth)
    |> ignore

let configureApp (app: IApplicationBuilder) =
    // Add Giraffe to the ASP.NET Core pipeline
    app
        .UseGiraffeErrorHandler(errorHandler)
        .UseAuthentication()
        .UseGiraffe webApp

let configureLogging (loggerBuilder: ILoggingBuilder) =
    loggerBuilder.AddConsole().AddDebug() |> ignore

[<EntryPoint>]
let main _ =
    Host
        .CreateDefaultBuilder()
        .ConfigureWebHostDefaults(fun webHostBuilder ->
            webHostBuilder
                .Configure(configureApp)
                .ConfigureServices(configureServices)
                .ConfigureLogging(configureLogging)
            |> ignore)
        .Build()
        .Run()

    0
